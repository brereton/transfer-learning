import numpy as np
import pandas as pd
from sys import getsizeof
import pickle
######
def feature_pairing(interactions, chemical, protein, score_filter, score_cut=[0,1], feature_expand = True):
    '''
    Function to generate dataframe of chemical and protein bitvectors and their corresponding interaction scores
    :param interactions: dataframe of interactions
    :param chemical: dataframe of chemicals
    :param protein: dataframe of proteins
    :param score_cut: cutoff of interaction score
    :return: dataframe of chemical and protein bitvectors as well as interaction scores
    '''
    if score_filter:
        interactions = interactions.iloc[np.where((interactions.loc[:,'combined_score'].values >= score_cut[0]) &
                                                  (interactions.loc[:,'combined_score'].values < score_cut[1]))[0],]
    # chemical_bitvec = [None] * interactions.shape[0]
    # protein_bitvec = [None] * interactions.shape[0]
    print(interactions.shape)

    if feature_expand:
        # feature_score_frame = pd.DataFrame(index=[str(iter) for iter in range(0, interactions.shape[0])],
        #                                    columns=range(0, len([char for char in protein['protein_feature_bitvec'][0]])+
        #                                                len([char for char in chemical['chemical_bitvec'][0]])), dtype='str')
        feature_list = [[None] for i in range(interactions.shape[0])]#[None] * interactions.shape[0]
        score_list = [None] * interactions.shape[0]
        # feature_list = []
        # score_list = []
        # feature_array = np.empty([interactions.shape[0],6532], dtype='int8')
    else:
        feature_score_frame = pd.DataFrame(columns=['chemical_bitvec', 'protein_feature_bitvec', 'score'],
                                           index=[str(iter) for iter in range(0, interactions.shape[0])], dtype='str')

    print('start')
    iter = 0
    for interaction_iter in interactions.index.values:
        # print(iter)
        # chemical_bitvec[iter] = chemical['chemical_bitvec'][interactions['chemical'][interaction_iter]]
        # protein_bitvec[iter] = protein['protein_feature_bitvec'][interactions['protein'][interaction_iter]]
       ##
        if feature_expand:
            # feature_score_frame.iloc[iter, :] = [char for char in chemical['chemical_bitvec'][interactions['chemical'][interaction_iter]]] +\
            #                                     [char for char in protein['protein_feature_bitvec'][interactions['protein'][interaction_iter]]]
            # feature_list.append([np.int(char) for char in chemical['chemical_bitvec'][interactions['chemical'][interaction_iter]]] +\
            #                     [np.int(char) for char in protein['protein_feature_bitvec'][interactions['protein'][interaction_iter]]])


            feature_list[iter] = [np.int(char) for char in chemical['chemical_bitvec'][interactions['chemical'][interaction_iter]]] +\
                                 [np.int(char) for char in protein['protein_feature_bitvec'][interactions['protein'][interaction_iter]]]
            # feature_list[iter] = chemical['chemical_bitvec'][interactions['chemical'][interaction_iter]] +\
            #                      protein['protein_feature_bitvec'][interactions['protein'][interaction_iter]]
            score_list[iter] = interactions['combined_score'][interaction_iter]

            # feature_list.append([np.int(char) for char in chemical['chemical_bitvec'][interactions['chemical'][interaction_iter]]] +\
            #                      [np.int(char) for char in protein['protein_feature_bitvec'][interactions['protein'][interaction_iter]]])
            # score_list.append(interactions['combined_score'][interaction_iter])

            # feature_array[iter] = [np.int(char) for char in chemical['chemical_bitvec'][interactions['chemical'][interaction_iter]]] +\
            #                      [np.int(char) for char in protein['protein_feature_bitvec'][interactions['protein'][interaction_iter]]]
        else:
            feature_score_frame.iloc[iter,:] = [chemical['chemical_bitvec'][interactions['chemical'][interaction_iter]],
                                                protein['protein_feature_bitvec'][interactions['protein'][interaction_iter]],
                                                interactions['combined_score'][interaction_iter]]
        ##
        # print(getsizeof(feature_list)/1e6)
        iter += 1

    return feature_list, score_list

def file_gen(interactions, chemical, protein, scorecut_list, max_chunksize, out_dir, file_commonname):
    '''
    Function to generate features files
    :param interactions: dataframe of interactions
    :param chemical: dataframe of chemicals
    :param protein: dataframe of proteins
    :param scorecut_list: list of score range pairs
    :param max_chunksize: maximum number of datapoints in each file
    :param out_dir: directory for outputing (storing) the files
    :param file_commonname: common part of the file names to be used for all the files
    :return: list of number of data points in each file
    '''
    fileiter = 0
    file_size = []
    for score_cut in scorecut_list:
        print('scores_'+str(score_cut))
        interactions_cut = interactions.iloc[np.where((interactions.loc[:,'combined_score'].values > score_cut[0]) &
                                                      (interactions.loc[:,'combined_score'].values <= score_cut[1]))[0],]
        print(str(interactions_cut.shape[0])+' datapoints')
        if interactions_cut.shape[0] > max_chunksize:
            print('chunked')
            chunk_list = [[iter, min(iter+int(max_chunksize), int(interactions_cut.shape[0]))] for iter in range(0,int(interactions_cut.shape[0]),int(max_chunksize))]
            for chunkiter in chunk_list:
                interactions_chunked = interactions_cut.iloc[chunkiter[0]:chunkiter[1],]
                feature_score_frame = feature_pairing(interactions = interactions_chunked,
                                                      chemical = chemical,
                                                      protein = protein,
                                                      score_filter = False)

                feature_score_frame.to_csv(out_dir+file_commonname+str(fileiter)+'.csv')
                file_size.append(interactions_chunked.shape[0])
                fileiter += 1
        elif interactions_cut.shape[0] > 0:
            print('not chunked')
            feature_score_frame = feature_pairing(interactions=interactions_cut,
                                                  chemical=chemical,
                                                  protein=protein,
                                                  score_filter=False)
            feature_score_frame.transpose()
            feature_score_frame.to_csv(out_dir + file_commonname + str(fileiter) + '.csv')
            file_size.append(interactions_cut.shape[0])
            fileiter += 1
        else:
            print('zero datapoint chunk')

    return file_size


# def feature_filtering(feature_score_frame, feature_expanded = True, feature_filterfrac = 1e-4):
#     if feature_expanded:
#         sum_feat = feature_score_frame.apply(np.sum, axis=0)
#         feature_score_frame = feature_score_frame.iloc[np.where(sum_feat < feature_score_frame.shape[0]*feature_filterfrac)[0],:]
#     else:
#         sample_chemicalbit = [char for char in feature_score_frame.iloc[1,'chemical_bitvec']]
#         sum_list = []
#         for bit_iter in range(len(sample_chemicalbit)):
#             sum_list.append(sum([np.int(char) for char in feature_score_frame.loc[:,'chemical_bitvec']]))



#####################
# max_chunksize = 50e3
# score_range = [800, 1000]
# score_step = 10
# scorecut_list = [[iter, iter+score_step] for iter in range(score_range[0], score_range[1], score_step)]

# file_commonname = 'interactions_score_'+str(score_range)+'_step_'+str(score_step)+'_chunked_'
out_dir = 'data/chunked/'
above_below = 'below'
interactions = pd.read_table('data/derived/filtered_interactions.tsv')
chemical = pd.read_table('data/derived/chemical_bitvecs.tsv', index_col=0)
protein = pd.read_table('data/derived/protein_bitvecs.tsv', index_col=0)


score_cut_step = 0.02
scorecut_list = [[np.quantile(interactions['combined_score'], q = quantile_cut),
                  np.quantile(interactions['combined_score'], q = quantile_cut+score_cut_step)] for quantile_cut in np.arange(0,1,score_cut_step)]
scorecut_list[(len(scorecut_list)-1)][1] = 1000
#####
# file_size = file_gen(interactions = interactions,
#                      chemical = chemical,
#                      protein = protein,
#                      scorecut_list = scorecut_list,
#                      max_chunksize = max_chunksize,
#                      out_dir = out_dir,
#                      file_commonname = file_commonname)


# feature_score_frame = feature_pairing(interactions = interactions,
#                                       chemical = chemical,
#                                       protein = protein,
#                                       score_filter = True,
#                                       score_cut = [965,1000],
#                                       feature_expand = True)
import time
start_time = time.time()

# feature_all = []
# score_all = []
for scorecut in scorecut_list:
    print(scorecut)
    feat_list, score_list = feature_pairing(interactions = interactions,
                                chemical = chemical,
                                protein = protein,
                                score_filter = True,
                                score_cut = scorecut,
                                feature_expand = True)


    with open(out_dir + 'featurlist_score' + str(scorecut) + '.pkl', 'wb') as f:
        pickle.dump(feat_list, f)

    with open(out_dir + 'scorelist_score' + str(scorecut) + '.pkl', 'wb') as f:
        pickle.dump(score_list, f)

    del feat_list
    del score_list
print("--- %s seconds ---" % (time.time() - start_time))



# from sys import getsizeof
# print(len(feat_list))
# print(getsizeof(feat_list)/1e6)

# feature_score_frame.shape
# feature_score_frame.size/1e6
# feature_score_frame = feature_score_frame.transpose()
# feature_score_frame[feature_score_frame.columns] = feature_score_frame[feature_score_frame.columns].astype('int32')

# feature_score_frame.to_csv('data/chunked/features_chemicalprotein_scores'+str(score_cut[0])+'to'+str(score_cut[1])+'.csv')

