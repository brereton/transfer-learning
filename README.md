# Installation

## Main Functionality

1. Install and source RD-Kit in the active python environment.  See: https://www.rdkit.org/docs/Install.html
2. Clone the repo from Gitlab: https://gitlab.com/ml-paper-group/transfer-learning
3. type `pip install -r requirements.txt`
4. type `pip install -e .`  
