import numpy as np
import random
import pandas as pd
###

def dataset_gen(interactions, chemical_info, protein_info, score_cut_positive, negative_ratio, seed_shuffling):
    '''
    Generating dataset for modeling based in the input label confidence range
    :param interactions: dataframe of chemical-protein interactions
    :param cheminal_info: dataframe of chemical space ot match the ids with the chemical annotations
    :param protein_info: dataframe of protein space ot match the ids with the protein annotations
    :param score_cut_positive: label confidence ranges to be used for selecting positive data points (true interactions)
    :param negative_ratio: ratio of number of negative data points to positive datapoints to be used for random negative sampling
    :param seed_shuffling: random seed for negative sampling
    :return: labels and ids for generating features
    '''
    positive_interactions = interactions.iloc[np.where((interactions['combined_score'] >= score_cut_positive[0]) &
                                                       (interactions['combined_score'] < score_cut_positive[1]))[0], :]

    positive_ids = feature_match(interactions = positive_interactions,
                                chemical_info = chemical_info,
                                protein_info = protein_info)
    #
    negative_interactions = negative_select(interactions = interactions,
                                            negative_num = len(positive_ids)*negative_ratio,
                                            negativeselection_seed = seed_shuffling)
    negative_ids = feature_match(interactions = negative_interactions,
                                 chemical_info = chemical_info,
                                 protein_info = protein_info)
    feature_ids = positive_ids + negative_ids
    labels = [1] * len(positive_ids) + [0] *len(negative_ids)

    return labels, feature_ids

def feature_match(interactions, chemical_info, protein_info):
    '''
    Function to generate list of matched ids between interactions and protein or chemical space for feature set generation
    :param interactions: dataframe of chemical-protein interactions
    :param cheminal_info: dataframe of chemical space ot match the ids with the chemical annotations
    :param protein_info: dataframe of protein space ot match the ids with the protein annotations
    :return: list of id pairs in chemical and protein spaces matched with the target interactions
    '''
    # feature_list = [None]*interactions.shape[0]
    id_list = [None]*interactions.shape[0]
    for interaction_iter in range(len(interactions.index.values)):
        id_list[interaction_iter] = [chemical_info['id'][interactions['chemical'].values[interaction_iter]],
                                     protein_info['id'][interactions['protein'].values[interaction_iter]]]

        # feature_list[interaction_iter] = chemical_info['chemical_bitvec'][interactions['chemical'].values[interaction_iter]]\
        #                                  + protein_info['protein_feature_bitvec'][interactions['protein'].values[interaction_iter]]

    return id_list

def negative_select(interactions, negative_num: int, negativeselection_seed: int):
    '''
    Generating negative data points randomly from random pairs that do not exist in the interactions dataframe
    :param interactions: dataframe of chemical-protein interactions
    :param negative_num: number of negative data points to be generated
    :param negativeselection_seed: random seed for negative sampling
    :return: dataframe of negative interactions
    '''
    random.seed(negativeselection_seed)

    interactions_len = interactions.shape[0]
    alloverlap_negative = pd.DataFrame({'chemical': interactions['chemical'].values[[random.randint(0,(interactions_len - 1)) for x in range(2*negative_num)]],
                                        'protein': interactions['protein'].values[[random.randint(0, (interactions_len - 1)) for x in range(2*negative_num)]]})

    negative_pairs = np.char.add(np.array(alloverlap_negative['chemical'].values, dtype=np.str),
                                 np.array(alloverlap_negative['protein'].values, dtype=np.str))
    potentialpositive_pairs = np.char.add(np.array(interactions['chemical'].values, dtype=np.str),
                                          np.array(interactions['protein'].values, dtype=np.str))
    alloverlap_negative['paired'] = list(negative_pairs)
    alloverlap_negative = alloverlap_negative.set_index('paired')
    alloverlap_negative = alloverlap_negative.drop(list(set(negative_pairs) & set(potentialpositive_pairs)), axis = 0)
    alloverlap_negative = alloverlap_negative.iloc[0:negative_num,]

    negatives = alloverlap_negative
    alloverlap_negative = None
    negative_pairs = None
    potentialpositive_pairs = None


    return negatives