import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt

import torch

from script.data_prep.FCNN_featureprep import dataset_gen
from script.train_test.FCNN_hypeparam_opt import FCNN_hyperparameter_optimizer
from script.nnet.VAE_net import VAE
from script.model.fcnn_savemodel import out_pickel_FCNN
####################################################
interactions = pd.read_table('data/derived/filtered_interactions.tsv')
chemical = pd.read_table('data/derived/chemical_bitvecs.tsv', index_col=0)
protein = pd.read_csv('data/derived/proteins_filteredbits_0.01percent_allstitch.csv', index_col=0)

chemical['id'] = range(0,chemical.shape[0])
protein['id'] = range(0,protein.shape[0])

with open('results/VAE_protein_results.pickle', 'rb') as handle:
    protein_VAE = pickle.load(handle)

with open('results/VAE_chemical_results.pickle', 'rb') as handle:
    chemical_VAE = pickle.load(handle)
###########
chemical_bitveclist = [iter.tolist() for iter in chemical_VAE['latent']]
protein_bitveclist = [iter.tolist() for iter in protein_VAE['latent']]

######
random_seed = {'batch_shuffling':42, 'negative_sampling':42,'nnet_init':42}
torch.manual_seed(random_seed['nnet_init'])
######
output_classnum = 2
test_range = [900,1000]
hyperparameter_list = [{'batch_size':batch,
                        'layers_sizes':layer_list,
                        'opt_parameteres':opt_params,
                        'epoch_num':epoch_num,
                        'score_cutoffs':score_cutoff}
                       for batch in [10000]
                       for layer_list in [[128, 64, 32, 16, 8]]
                       for opt_params in [{'method': 'Adam','learning_rate': 0.0005}] #'momentum': 0.9
                       for epoch_num in [[100]]
                       for score_cutoff in [[[389, 473]]]]
####################################################
# Running the model
####################################################
import time
start_time = time.time()
labels_testing, feature_ids = dataset_gen(interactions = interactions,
                                          chemical_info = chemical,
                                          protein_info = protein,
                                          score_cut_positive = test_range,
                                          negative_ratio = 1,
                                          seed_shuffling = random_seed['negative_sampling'])

data_testing = [np.float_(chemical_bitveclist[feat_iter[0]] + protein_bitveclist[feat_iter[1]]) for feat_iter in feature_ids]

test_set = {'features_test':data_testing,'lables_test': labels_testing}

input_featnum = len([np.int(char) for char in chemical['chemical_bitvec'][0]] +\
                    [np.int(char) for char in protein['protein_feature_bitvec'][0]])

print('start')
network_trained, hyperparams, layer_values_hyperparam, performance_hyperparam_dict, loss_hyperparam_dict = FCNN_hyperparameter_optimizer(hyperparams = hyperparameter_list,
                                                                                                                                         test_set = test_set,
                                                                                                                                         interactions = interactions,
                                                                                                                                         chemical_feat = chemical_bitveclist,
                                                                                                                                         protein_feat = protein_bitveclist,
                                                                                                                                         cheminal_info = chemical,
                                                                                                                                         protein_info = protein,
                                                                                                                                         output_classnum = output_classnum,
                                                                                                                                         negative_ratio = 1,
                                                                                                                                         seed_dict = random_seed,
                                                                                                                                         testing_whiletraining = True)

print("--- %s seconds ---" % (time.time() - start_time))
##
out_pickel_FCNN(net = network_trained,
                performance = performance_hyperparam_dict,
                layers_values = layer_values_hyperparam,
                loss = loss_hyperparam_dict,
                hyperparameter_list = hyperparams,
                test_range = test_range,
                random_seed = random_seed)






