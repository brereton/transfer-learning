import numpy as np
import torch
from torch.autograd import Variable
###

def latent_extract(network, features):
    '''
    Extrating embeddings
    :param network: trained neural network
    :param features: list of features
    :return: list of embeddings
    '''
    latent_list = []
    for feat_iter in range(0, len(features)):
        # print(feat_iter)
        data = torch.tensor([np.float_(features[feat_iter])], dtype=torch.float)  # torch.tensor(features_expandlist, dtype=torch.bool)
        data = Variable(data)
        data = data.view(data.size(0), -1)
        recon_batch, mu, logvar = network(data.float())
        std = torch.exp(0.5 * logvar)
        eps_val = torch.full_like(mu, fill_value=0 * 0.01)
        z_val = eps_val.mul(std).add_(mu)
        latent_list.append(np.array(z_val[0].data))

    return latent_list