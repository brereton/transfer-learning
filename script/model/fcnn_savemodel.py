import pickle
###

def out_pickel_FCNN(net, performance, layers_values, loss, hyperparameter_list, test_range, random_seed):
    '''
    Saving the results of neural network modeling (FTM or non-FTM)
    :param net: trained network
    :param performance: performance dictionary across tested hyperparameter sets
    :param layers_values: layer values per epoch
    :param loss: loss dictionary across tested hyperparameter sets
    :param hyperparameter_list: hyperparameter sets
    :param test_range: label confidence range used for test set
    '''
    out_dic = {'performance': performance,
               'network': net,
               'layer_values': layers_values,
               'loss': loss,
               'model_parameters': hyperparameter_list}

    with open('results/FCNN_' + 'cutoffs'+str(hyperparameter_list[0]['score_cutoffs'])+'_epoch'+str(hyperparameter_list[0]['epoch_num'])+
              '+lr'+str(str(hyperparameter_list[0]['opt_parameteres']['learning_rate']))+'_testrange'+str(test_range)+'_'+str(random_seed)+'results.pickle', 'wb') as handle:
        pickle.dump(out_dic, handle, protocol=pickle.HIGHEST_PROTOCOL)