import numpy as np
import matplotlib.pyplot as plt


def onestepnet_plot(loss_indcut, performance_indcut,
                    lowcutoffs, highcutoffs,
                    confidence_scores, startepoch, endepoch,
                    loss_min, performance_max):
    '''
    generating loss and performance plots of neural network models trained on different label confidence ranges
    :param filters: dictionary of confidence ranges
    :param confidence_scores: array of confidence scores of the data points in the target dataset
    :param startepoch: starting epoch to be used for plottting the loss and performance
    :param endepoch: last epoch to be used for plottting the loss and performance
    '''
    fig, axs = plt.subplots(len(lowcutoffs), 3, gridspec_kw={'hspace': 0.17, 'wspace': 0.28}, figsize=(11, 3+2*len(lowcutoffs)))
    plt.rcParams.update({'font.size': 12})
    # plotting distributions of positive pairs based on each confidence cutoff
    for confidence_iter in range(0, len(lowcutoffs)):
        axs[confidence_iter, 0].hist(
            confidence_scores[np.where((confidence_scores >= lowcutoffs[confidence_iter]) &
                                       (confidence_scores < highcutoffs[confidence_iter]))[0]],
            bins=50, range=(300, 1000), color=(227/255,26/255,28/255))
        axs[confidence_iter, 0].set_xlim(200, 1000)
        axs[confidence_iter, 0].set_ylim(0, 3e5)
        axs[confidence_iter, 0].set(ylabel='Frequency')

    axs[0, 0].set_title('(A) confidence range for training', pad=15)
    axs[-1, 0].set(xlabel='label confidence score')
    # plotting loss per epoch based on each confidence cutoff
    for confidence_iter in range(0, len(lowcutoffs)):
        axs[confidence_iter, 1].plot(np.arange(startepoch, endepoch),
                                     loss_indcut[str(lowcutoffs[confidence_iter])+ '-'+str(highcutoffs[confidence_iter])][
                                     startepoch:endepoch], c='black')
        axs[confidence_iter, 1].hlines(y=loss_min, xmin=startepoch, xmax=endepoch,
                                       linestyles='dashed')
        axs[confidence_iter, 1].set_ylim(5.0, 7.0)
        axs[confidence_iter, 1].set(ylabel='normalized loss (1e-5)')

    axs[0, 1].set_title('(B) loss (individual models)', pad=15)
    axs[-1, 1].set(xlabel='epochs')
    # plotting performance per epoch based on each confidence cutoff
    for confidence_iter in range(0, len(lowcutoffs)):
        axs[confidence_iter, 2].plot(np.arange(startepoch, endepoch),
                                     performance_indcut[str(lowcutoffs[confidence_iter])+ '-'+str(highcutoffs[confidence_iter])][
                                     startepoch:endepoch], c='black')
        axs[confidence_iter, 2].hlines(y=performance_max, xmin=startepoch, xmax=endepoch,
                                       linestyles='dashed')
        axs[confidence_iter, 2].set_ylim(60, 80)
        axs[confidence_iter, 2].set(ylabel='accuracy (%)')

    axs[0, 2].set_title('(C) accuracy (individual models)', pad=15)
    axs[-1, 2].set(xlabel='epochs')


    # fig.savefig('figures/FCNN_IndModels_loss_performance_'+str(list(performance_indcut.keys()))+'_withconfidencehist.png')
    fig.show()