import numpy as np
import torch
from torch.autograd import Variable
from script.nnet.VAE_loss import loss_function
###
def training_vae(network_train, features, batch_size, epochs, optimizer):
    '''
    Training neural network
    :param network_train: network to be trained
    :param features: list of features to be used in variational autoencoder
    :param batch_size: batch size
    :param epochs: number of epochs
    :param optimizer: optimizer
    :return: trained network, layer weights and loss per epoch
    '''
    indices_shufflled = list(range(len(features)))

    featureids_shuffled = [iter for iter in indices_shufflled]
    batch_ids = [[iter*batch_size, min(len(features), (iter+1)*batch_size)] for iter in range(int(len(features)/batch_size)+1)]

    loss_training = []
    layers_values = []
    for epoch in range(epochs):
        print('epoch {} '.format(epoch+1))
        #
        layerval_tmp = [network_train.layers[layer_iter].weight.tolist() for layer_iter in np.arange(0,len(network_train.layers))]
        layers_values.append(layerval_tmp)

        loss_sum = 0.0
        for batch_idx in range(len(batch_ids)):
            features_expandlist = [np.float_(features[featureids_shuffled[feat_iter]]) for feat_iter in range(batch_ids[batch_idx][0], batch_ids[batch_idx][1])]

            data = torch.tensor(features_expandlist, dtype=torch.float) #torch.tensor(features_expandlist, dtype=torch.bool)
            data= Variable(data)
            data = data.view(data.size(0), -1)

            optimizer.zero_grad()
            recon_batch, mu, logvar = network_train(data.float())

            loss = loss_function(recon_batch, data, mu, logvar)
            loss_sum += loss.item()

            loss.backward()
            optimizer.step()

        loss_training.append(loss_sum/len(features))
        print(np.log10(loss_training[-1]))
        if len(loss_training) > 2:
            print(1000.0*abs((loss_training[-1]-loss_training[-2])/loss_training[-1]))

    return network_train, layers_values, loss_training