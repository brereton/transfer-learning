import numpy as np
import matplotlib.pyplot as plt
###

def FTL_plot(loss_transfer, performance_transfer,
             confidence_scores, epoch_transfer,
             cutoffs, best_loss,
             best_performance, startepoch, endepoch):
    '''
    generating loss and performance plots of FTLs trained on different label confidence ranges
    :param loss_transfer:
    :param performance_transfer:
    :param confidence_scores:
    :param epoch_transfer:
    :return:
    '''
    fig, axs = plt.subplots(len(loss_transfer), 3, gridspec_kw={'hspace': 0.17, 'wspace': 0.25}, figsize=(11, 3+2*len(loss_transfer)))
    plt.rcParams.update({'font.size': 12})
    # plotting distributions of positive pairs based on each confidence cutoff
    for confidence_iter in range(0, len(loss_transfer)):
        axs[confidence_iter, 0].hist(
            confidence_scores[np.where((confidence_scores >= cutoffs[confidence_iter][0][0]) &
                                       (confidence_scores < cutoffs[confidence_iter][0][1]))[0]],
            bins=np.arange(300, 1005, 5).tolist(), range=(300, 1000), color=(254/255, 178/255, 76/255))
        axs[confidence_iter, 0].hist(confidence_scores[np.where((confidence_scores >= cutoffs[confidence_iter][1][0]) &
                                                                (confidence_scores < cutoffs[confidence_iter][1][1]))[0]],
                                     bins=np.arange(300, 1005, 5).tolist(), range=(300, 1000), color=(227/255,26/255,28/255))
        axs[confidence_iter, 0].set_xlim(300, 1000)
        axs[confidence_iter, 0].set_ylim(0, 1.3e5)
        axs[confidence_iter, 0].set(ylabel='Frequency')

    axs[0, 0].set_title('(A) confidence range for training', pad=15)
    axs[-1, 0].set(xlabel='label confidence score')
    # plotting loss per epoch based on each confidence cutoff
    for confidence_iter in range(0, len(loss_transfer)):
        axs[confidence_iter, 1].plot(np.arange(startepoch, epoch_transfer[confidence_iter][1]-1),
                                     loss_transfer[confidence_iter][startepoch:(epoch_transfer[confidence_iter][1]-1)],
                                     c=(254/255, 178/255, 76/255))
        axs[confidence_iter, 1].plot(np.arange(epoch_transfer[confidence_iter][1]-1, endepoch),
                                     loss_transfer[confidence_iter][(epoch_transfer[confidence_iter][1]-1):endepoch],
                                     c=(227/255,26/255,28/255))
        axs[confidence_iter, 1].hlines(y=best_loss, xmin=epoch_transfer[confidence_iter][0], xmax=epoch_transfer[confidence_iter][-1],
                                       linestyles='dashed')
        axs[confidence_iter, 1].set_xlim(startepoch, endepoch)
        axs[confidence_iter, 1].set_ylim(5.0, 6.0)
        axs[confidence_iter, 1].set(ylabel='normalized loss (1e-5)')

    axs[0, 1].set_title('(B) loss (2-stpe FTL)', pad=15)
    axs[-1, 1].set(xlabel='epochs')
    # plotting performance per epoch based on each confidence cutoff
    for confidence_iter in range(0, len(loss_transfer)):
        axs[confidence_iter, 2].plot(np.arange(startepoch, epoch_transfer[confidence_iter][1]-1),
                                     performance_transfer[confidence_iter][startepoch:(epoch_transfer[confidence_iter][1]-1)],
                                     c=(254/255, 178/255, 76/255))
        axs[confidence_iter, 2].plot(np.arange(epoch_transfer[confidence_iter][1]-1, endepoch),
                                     performance_transfer[confidence_iter][(epoch_transfer[confidence_iter][1]-1):endepoch],
                                     c=(227/255,26/255,28/255))
        axs[confidence_iter, 2].hlines(y=best_performance, xmin=epoch_transfer[confidence_iter][0], xmax=epoch_transfer[confidence_iter][-1],
                                       linestyles='dashed')
        axs[confidence_iter, 2].set_xlim(startepoch, endepoch)
        axs[confidence_iter, 2].set_ylim(72, 80)
        axs[confidence_iter, 2].set(ylabel='accuracy (%)')

    axs[0, 2].set_title('(C) accuracy (2-stpe FTL)', pad=15)
    axs[-1, 2].set(xlabel='epochs')
    #

    fig.savefig('figures/FCNN_Transfer_vs_BestOfIndModels_loss_performance_FTL_'+str(cutoffs)+'_withconfidencehist.png')
    fig.show()