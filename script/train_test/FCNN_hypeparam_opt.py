import numpy as np

from script.train_test.transfer_model import transfer_train
from script.train_test.FCNN_testing import FCNN_testing
###

def FCNN_hyperparameter_optimizer(hyperparams, test_set,
                                  interactions, chemical_feat, protein_feat,
                                  cheminal_info, protein_info,
                                  negative_ratio, seed_dict,
                                  output_classnum,testing_whiletraining = False):
    '''
    Function for hyperparameter optimization of the neural network models
    :param hyperparams: list of hyperparameters to be explored
    :param test_set: dataset generated for testing the model
    :param interactions: dataframe of chemical-protein interactions
    :param chemical_feat: list of features of chemical space
    :param protein_feat: list of features of protein space
    :param cheminal_info: dataframe of chemical space ot match the ids with the chemical annotations
    :param protein_info: dataframe of protein space ot match the ids with the protein annotations
    :param negative_ratio: ratio of number of negative data points to positive datapoints to be used for random negative sampling
    :param seed_dict: dictionary of random seeds to be used for 'batch_shuffling' and 'negative_sampling'
    :param output_classnum: number of classes in the output variable
    :param testing_whiletraining: logical parameter (if True, testing is done per epoch to follow the trend of traingina and testing per epoch)
    :return: trained network, list of hyperparameters, layer weights per epochs, loss and performance for training and testing
    '''

    loss_list_training = []
    loss_list_test = []
    performance_list_training = []
    performance_list_test = []

    network_trained = {}
    layer_values_hyperparam = {}
    for hyper_iter in range(0,len(hyperparams)):
        print(hyperparams[hyper_iter])
        #
        batch_size = hyperparams[hyper_iter]['batch_size']
        layer_list = hyperparams[hyper_iter]['layers_sizes']
        optimizer_parameters = hyperparams[hyper_iter]['opt_parameteres']
        epoch_list = hyperparams[hyper_iter]['epoch_num']
        score_cutoff_list = hyperparams[hyper_iter]['score_cutoffs']

        net_trained, layers_train, loss_training, performance_training = transfer_train(layer_list = layer_list,
                                                                                        optimizer_params = optimizer_parameters,
                                                                                        batch_size = batch_size,
                                                                                        interactions = interactions,
                                                                                        chemical_feat = chemical_feat,
                                                                                        protein_feat = protein_feat,
                                                                                        cheminal_info = cheminal_info,
                                                                                        protein_info = protein_info,
                                                                                        negative_ratio = negative_ratio,
                                                                                        score_cutoff_list = score_cutoff_list,
                                                                                        epoch_list = epoch_list,
                                                                                        seed_dict = seed_dict,
                                                                                        test_set = test_set,
                                                                                        output_classnum = output_classnum,
                                                                                        testing_whiletraining = testing_whiletraining)
        #
        performance, test_loss = FCNN_testing(network_test=net_trained,
                                              features_testing = test_set['features_test'],
                                              labels_testing = test_set['lables_test'],
                                              batch_size = batch_size)
        #
        network_trained[hyper_iter] = net_trained
        layer_values_hyperparam['Hyperparameter ' + str(hyper_iter)] = layers_train
        loss_list_training.append(loss_training)
        performance_list_training.append(performance_training)
        loss_list_test.append(test_loss)
        performance_list_test.append(performance)


    loss_hyperparam_dict = {'training_perepoch': loss_list_training, 'testing': loss_list_test}
    performance_hyperparam_dict = {'training_perepoch': performance_list_training, 'testing': performance_list_test}
    print('best hyperparameter set is {} resulting in {} performance.'.format(hyperparams[np.argmax(performance_list_test)], np.max(performance_list_test)))
    return network_trained, hyperparams, layer_values_hyperparam, performance_hyperparam_dict, loss_hyperparam_dict
