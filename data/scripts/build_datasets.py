import gzip
import collections
import pandas as pd
import numpy as np
import logging
import sys
from Bio import SwissProt
from rdkit import Chem
from rdkit.Chem import AllChem


# Short fix due to biopython error resulting from new swissprot file convention change (Dec 2019)
# https://github.com/biopython/biopython/issues/2417
def read_ft(record, line):
    return


SwissProt._read_ft = read_ft

# Input Files
CHEMICAL_LINKS = '../raw/9606.protein_chemical.links.v5.0.tsv.gz'
CHEMICALS = '../raw/chemicals.v5.0.tsv.gz'
PROTEOME = '../raw/uniprot-human-20200218.txt.gz'

# Output Files:
PROTEIN_BITMAP_INDEX = '../derived/protein_bitvec_index.tsv.gz'
PROTEIN_BITMAPS = '../derived/protein_bitvecs.tsv.gz'
COMPOUND_BITMAPS = '../derived/chemical_bitvecs.tsv.gz'
INTERACTIONS = '../derived/filtered_interactions.tsv.gz'
RUN_LOG = '../derived/generate_datasets.log'

# Set up log file
logger = logging.getLogger('build_dataset')
hdlr = logging.FileHandler(RUN_LOG)
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.addHandler(logging.StreamHandler(sys.stdout))
logger.setLevel(logging.INFO)

# Get a list of interactions to avoid unnecessarily featurinzing proteins or compounds
logger.info(f'**********************')
logger.info(f'Reading Interactions Dataframe: {CHEMICAL_LINKS}.')
df_interactions = pd.read_csv(CHEMICAL_LINKS, compression='gzip', sep='\t', quotechar='"')
logger.info(f'  Complete.  --> Shape: {df_interactions.shape}')
logger.info(df_interactions.head())
nr_compounds = set(df_interactions['chemical'])
nr_proteins = set(df_interactions['protein'])
logger.info(f'  Non-Redundant Compounds: {len(nr_compounds)}')
logger.info(f'  Non-Redundant Proteins: {len(nr_proteins)}')

# Get a list of interactions to avoid unnecessarily featurinzing proteins or compounds
logger.info(f'**********************')
logger.info(f'Building PFAM Map from ENS IDs using file {PROTEOME}.')
string_to_pfams = collections.defaultdict(set)
all_pids = set()
with gzip.open(PROTEOME) as f:
    for sp in SwissProt.parse(f):
        string_ids = [x[1] for x in sp.cross_references if x[0] == 'STRING']
        pfam_ids = [x[1] for x in sp.cross_references if x[0] == 'Pfam']
        for sid in string_ids:
            if sid in nr_proteins:
                for pid in pfam_ids:
                    string_to_pfams[sid].add(pid)
                    all_pids.add(pid)
logger.info(f"  Total String IDs Found: {len(string_to_pfams)}")
logger.info(f"  Total Pfam IDs Found: {len(all_pids)}")

# Pull out the PFAM domains into binary vectors
logger.info(f'**********************')
logger.info(f'Creating an index of pfam ids as a bit vector.')
all_pids_n = len(all_pids)
pid_list = list(all_pids)
pid_index = {}
for i, pid in enumerate(pid_list):
    pid_index[pid] = i
string_to_pfams.items()
df_pfam = pd.DataFrame(pid_index.items(), columns=['pfam_id', 'protein_feature_bitvec_index'])
df_pfam.to_csv(PROTEIN_BITMAP_INDEX, sep='\t', index=False, compression='gzip')
logger.info(f'  Pfam Index written to file: {PROTEIN_BITMAP_INDEX}')

# Write out the ensemble to pfam bitvec maps (protein feature vectors)
logger.info(f'**********************')
logger.info(f'Converting protein pfam bit vector to string and writing to feature file.')
data_array = []
used_proteins = set()
for sid, pid_set in string_to_pfams.items():
    bin_traits = np.zeros(all_pids_n, dtype=bool)
    for pid in pid_set:
        bin_index = pid_index[pid]
        bin_traits[bin_index] = True
    format_bit_vec = ''.join([str(int(x)) for x in bin_traits])
    data_array.append((sid, format_bit_vec))
    used_proteins.add(sid)
df_protein = pd.DataFrame(data_array, columns=['protein', 'protein_feature_bitvec'])
df_protein.to_csv(PROTEIN_BITMAPS, sep='\t', index=False, compression='gzip')
logger.info(f'  Uniprot Ensemble IDs From Stitch Set: {len(used_proteins)}')
logger.info(f'  Protein features written to file: {PROTEIN_BITMAPS}')


# Generating the fingerprints for relevant compounds:
# RDKit Morgan2 is similar to ECFP4: https://www.rdkit.org/UGM/2012/Landrum_RDKit_UGM.Fingerprints.Final.pptx.pdf
logger.info(f'**********************')
logger.info(f"Streaming Compounds from {CHEMICALS} to filter out those with human interactions.")
total_cpds = 0
human_interactions = 0
rdkit_errors = 0
valid_cpds = []
used_compounds = set()
with gzip.open(CHEMICALS, 'rt') as F:
    header = next(F).strip().split('\t')
    logger.info(f'Header Found: {header}')
    for line in F:
        cid, name, mw, smi = line.strip().split('\t')
        total_cpds += 1
        if cid in nr_compounds:
            try:
                m = Chem.MolFromSmiles(smi)
                fp = AllChem.GetMorganFingerprintAsBitVect(m, 2, nBits=1024)
                valid_cpds.append((cid, smi, fp.ToBitString()))
                used_compounds.add(cid)
            except Exception:
                rdkit_errors += 1
        if total_cpds % 10000 == 0:
            logger.info(f"  Parsed {total_cpds} Total Compounds.")
logger.info(f"  Parsed {total_cpds} Total Compounds.")
logger.info(f"  Identified {len(used_compounds)} Compounds With Human Interactions.")
logger.info(f"  Identified {rdkit_errors} Compounds With RD-KIT Errors.")
df_compounds = pd.DataFrame(valid_cpds, columns=['chemical', 'smiles', 'chemical_bitvec'])
df_compounds.to_csv(COMPOUND_BITMAPS, sep='\t', index=False, compression='gzip')
logger.info(f'  Writen to file: {COMPOUND_BITMAPS}')

# Rewrite interactions file
logger.info(f'**********************')
logger.info(f'Filtering out interactions without protein or chemical bitmaps.')
logger.info(f'  Original   --> Shape: {df_interactions.shape}')
df_filtered = df_interactions.loc[df_interactions['chemical'].isin(used_compounds)
                                  & df_interactions['protein'].isin(used_proteins)]
logger.info(f'  Complete.  --> Shape: {df_filtered.shape}')
logger.info(df_filtered.head())
df_filtered.to_csv(INTERACTIONS, sep='\t', index=False, compression='gzip')
logger.info(f'  Writen to File: {INTERACTIONS}')
