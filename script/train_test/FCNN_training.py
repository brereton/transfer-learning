import numpy as np
import random

import torch
import torch.nn as nn
from torch.autograd import Variable

from script.train_test.FCNN_testing import FCNN_testing
###

def FCNN_training(network_train, feature_ids, chemical_feat, protein_feat,
                  labels_training, batch_size, epochs,
                  optimizer, seed_shuffling,
                  test_set = [], testing_whiletraining=False):
    '''
    Training neural network
    :param network_train: network to be used for training
    :param feature_ids: list of feature id pairs from chemical and protein spaces matched with the training interactions
    :param chemical_feat: list of chemical features
    :param protein_feat: list of protein features
    :param labels_training: labels of trainign datapoints
    :param batch_size: batch size for training
    :param epochs: number of epochs
    :param optimizer: optimizer
    :param seed_shuffling: random seed to be used for shuffling data points for batch selection
    :param test_set: testing set
    :param testing_whiletraining: logical parameter (if True, testing is done per epoch to follow the trend of traingina and testing per epoch)
    :return: trained network, layer weights per epochs, loss and performance for training and testing
    '''

    input_featnum = len(chemical_feat[0] + protein_feat[0])
    #
    random.seed(seed_shuffling)
    indices_shufflled = list(range(len(labels_training)))
    random.shuffle(indices_shufflled)


    labels_shuffled = [labels_training[iter] for iter in indices_shufflled]
    featureids_shuffled = [feature_ids[iter] for iter in indices_shufflled]

    batch_ids = [[iter*batch_size, min(len(labels_shuffled), (iter+1)*batch_size)] for iter in range(int(len(labels_shuffled)/batch_size)+1)]

    loss_training = []
    loss_testing = []

    performance_training = []
    performance_testing = []

    layers_values = []

    criterion = nn.NLLLoss()
    for epoch in range(epochs):
        print('epoch {} '.format(epoch+1))
        #
        layerval_tmp = [network_train.layers[layer_iter].weight.tolist() for layer_iter in np.arange(0,len(network_train.layers))]

        layers_values.append(layerval_tmp)
        layerval_tmp = []

        loss_sum = 0.0
        correct = 0.0
        for batch_idx in range(len(batch_ids)):


            features_expandlist = [np.float_(chemical_feat[featureids_shuffled[feat_iter][0]] + protein_feat[featureids_shuffled[feat_iter][1]]) for feat_iter in
                              range(batch_ids[batch_idx][0], batch_ids[batch_idx][1])]

            labels_batch = list(np.array(labels_shuffled)[range(batch_ids[batch_idx][0], batch_ids[batch_idx][1])])

            data = torch.tensor(features_expandlist, dtype=torch.float)
            target = torch.tensor(labels_batch, dtype=torch.long)

            data, target = Variable(data), Variable(target)
            data = data.view(-1, input_featnum)
            optimizer.zero_grad()
            net_out = network_train(data.float())

            loss = criterion(net_out, target)

            loss_sum += loss.item()
            loss.backward()
            optimizer.step()

            pred = net_out.data.max(1)[1]  # get the index of the max log-probability
            correct += pred.eq(target.data).sum()

        print(correct.item())
        loss_training.append(loss_sum/(np.float(len(labels_shuffled)))) #
        performance_training.append(100. * correct.item() / len(labels_training))
        print(loss_training[-1])

        # apply the model on test set
        if testing_whiletraining:
            test_performance, test_loss = FCNN_testing(network_test = network_train,
                                                       features_testing = test_set['features_test'],
                                                       labels_testing = test_set['lables_test'],
                                                       batch_size = batch_size)
            loss_testing.append(test_loss)
            print('Test loss: {}'.format(test_loss))

            performance_testing.append(test_performance)

            print('Test performance: {}'.format(test_performance))
            if epoch > 0:
                print('test loss change: {}'.format((loss_testing[-1]-loss_testing[-2]) / loss_testing[-1]))
                print('test performance change: {}'.format((performance_testing[-1] - performance_testing[-2]) / performance_testing[-1]))


    loss_dict = {'training_perepoch': loss_training, 'testing_perepoch': loss_testing}
    performance_dict = {'training_perepoch': performance_training, 'testing_perepoch': performance_testing}

    return network_train, layers_values, loss_dict, performance_dict