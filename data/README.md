# Dataset Downloads
This folder contains download instructions for dataset downloads and scripts used 
to generate the relevant training data.

## Folder Layout
* Subfolder `raw/` is used to store the original files downloaded directly from the source
* Subfolder `scripts/` is used to training data scripts
* Subfolder `derived/` is used to store files created by training data scripts

## Datasource: Stitch
The stitch database is used for positive training data examples.  This toy dataset is limited to human only.
Datasets must be downloaded externally due to file sizes and static nature of datasets.    
1. Go to http://stitch.embl.de/cgi/download.pl 
2. Select organism -> human and click update
3. Download `9606.protein_chemical.links.v5.0.tsv.gz` to subfolder `raw/` (71 MB)
4. Download `chemicals.v5.0.tsv.gz` to subfolder `raw/` (2.2 GB)

## Datasource: Uniprot
The uniprot human proteome is used for mapping ensemble ids onto pfam domains to create protein feature vectors similar 
to those used in SecureDTI.  The file `raw/uniprot-human-20200218.txt.gz` was downloaded from url
https://www.uniprot.org/uniprot/?query=organism%3A%22Homo+sapiens+%28Human%29+%5B9606%5D%22&sort=score&format=txt&compress=yes
on Feb 18, 2020.  Since the query may change over time, the snapshot used in the study is provided in the repo.

## Derived Files
The once the three necessary files are available in subfolder `raw/`, the script `scripts/build_datasets.py` is used to 
regenerate protein and ligand feature vectors.  
* `derived/generate_datasets.log` - Run log from when the committed datasets were generated on Feb 18, 2020.
* `derived/protein_bitvec_index.tsv.gz` - Mapping files of the pfam domains to feature bitvec indices.
* `derived/protein_bitvec.tsv.gz` - Feature vectors for each ensp protein seen in the human interactions file.
* `derived/chemicals_bitvec.tsv.gz` - Smiles strings and feature vectors for each chemical in the human interactions file.
* `derived/filtered_interactions.tsv.gz` - A copy of the stitch interactions file, but excluding records for which the
chemical or protein could not be used to generate a feature vector.  See `build_datasets.py` and `generate_datasets.log`
for more information. 