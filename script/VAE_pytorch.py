import time
import torch

from script.nnet.VAE_optimizer import model_init
from script.train_test.VAE_training import training_vae
from script.data_prep.space_initialization import space_init
from script.model.vae_latentvar import latent_extract
####################################################
target_space = 'chemical'
batch_size = 1000
epoch_num = 5
random_seed = {'nnet_init':42}
optimizer_params = {'method': 'Adam','learning_rate': 0.0001}

##
model_parameters = {'target_space': target_space,
                    'batch_size': batch_size,
                    'epoch_num': epoch_num,
                    'random_seed': random_seed,
                    'optimizer_params': optimizer_params}

bitvec_list, layer_list = space_init(target_space)
input_featnum = len(bitvec_list[0])

torch.manual_seed(random_seed['nnet_init'])
net, optimizer = model_init(input_size=input_featnum,
                            layers_size=layer_list,
                            optimizer_params=optimizer_params)

network, layers_values, loss_training = training_vae(network_train = net,
                                                     features = bitvec_list,
                                                     batch_size = batch_size,
                                                     epochs = epoch_num,
                                                     optimizer = optimizer)
##
latent = latent_extract(network = network, features = bitvec_list)



