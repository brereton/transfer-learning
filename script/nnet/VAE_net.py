import torch
import torch.nn.functional as F
from torch import nn
###

class VAE(nn.Module):

    def __init__(self, input_size, layers_size):
        '''
        Function for building the network
        :param input_size: number of input features
        :param layers_size: sizes of the hidden layers
        '''
        super(VAE, self).__init__()

        layers_decoder = layers_size[::-1]
        self.layers_size = layers_size
        self.input_size = input_size
        self.layers_decoder = layers_decoder

        self.layers = [nn.Linear(input_size, layers_size[0])]
        for i in range(1,len(layers_size)):
            self.layers.append(nn.Linear(layers_size[i-1], layers_size[i]))
        #
        self.layers.append(nn.Linear(layers_size[len(layers_size)-2], layers_size[len(layers_size)-1]))
        #
        for i in range(1,len(layers_decoder)):
            self.layers.append(nn.Linear(layers_decoder[i-1], layers_decoder[i]))
        #
        self.layers.append(nn.Linear(layers_decoder[-1], input_size))

    def encode(self ,x):
        for i in range(0, len(self.layers_size) - 1):
            x = F.relu(self.layers[i](x))
        mu = self.layers[len(self.layers_size)-1](x)
        logvar = self.layers[len(self.layers_size)](x)
        return mu, logvar

    def decode(self ,z):
        for i in range(len(self.layers_size)+1, len(self.layers) - 1):
            z = F.relu(self.layers[i](z))
        out = torch.sigmoid(self.layers[len(self.layers)-1](z))
        return out

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return eps.mul(std).add_(mu)

    def forward(self ,x):
        mu, logvar = self.encode(x)
        z = self.reparameterize(mu, logvar)
        decode_z = self.decode(z)
        return decode_z, mu, logvar
