import torch.nn as nn
import torch.nn.functional as F
###

class FCNN(nn.Module):

    def __init__(self, input_size, num_layers, layers_size, output_size):
        '''
        Function for building the network
        :param input_size: number of input features
        :param num_layers: number of the hidden layers
        :param layers_size: sizes of the hidden layers
        :param output_size: number of output predictions
        '''
        super(FCNN, self).__init__()

        self.layers = [nn.Linear(input_size, layers_size[0])]
        for i in range(1,num_layers):
            self.layers.append(nn.Linear(layers_size[i-1], layers_size[i]))
        self.layers.append(nn.Linear(layers_size[-1], output_size))
    ###########################################
    # Forward passing in the network
    # implementation of activation
    # functions in each layer
    ###########################################
    def forward(self, x):
        for i in range(0, len(self.layers) - 1):
            x = F.relu(self.layers[i](x))
        x = self.layers[-1](x)
        return F.log_softmax(x, dim = 1)