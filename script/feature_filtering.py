import numpy as np
import pandas as pd
import csv


interactions = pd.read_table('data/derived/filtered_interactions.tsv')
chemical = pd.read_table('data/derived/chemical_bitvecs.tsv', index_col=0)
protein = pd.read_table('data/derived/protein_bitvecs.tsv', index_col=0)

feature_list = [None] * interactions.shape[0]

interaction_iter = 0
featurestr_list = list(chemical['chemical_bitvec'][interactions['chemical'].values[interaction_iter]] + \
              protein['protein_feature_bitvec'][interactions['protein'].values[interaction_iter]])

feature_samplenum = [0]*len(featurestr_list)
for interaction_iter in range(len(interactions.index.values)): #
    print(interaction_iter)
    featurestr_list = list(chemical['chemical_bitvec'][interactions['chemical'].values[interaction_iter]] +protein['protein_feature_bitvec'][interactions['protein'].values[interaction_iter]])

    feature_samplenum = [feature_samplenum[iter]+(featurestr_list[iter] == '1') for iter in range(len(featurestr_list))]


interactuion_num = interactions.shape[0]

nonrare_feat = []
for feat_iter in range(len(feature_samplenum)):
    if(feature_samplenum[feat_iter]/interactuion_num > 0.0001):
        nonrare_feat.append(feat_iter)

print('percentage of non-zero bits = {}'.format(len(np.nonzero(feature_samplenum)[0])/len(feature_samplenum)))


with open('data/raw/bits_samplenum_allstitch.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(feature_samplenum)


chemical_featnum = len(chemical['chemical_bitvec'][interactions['chemical'].values[interaction_iter]])
nonrare_proteinbits = []
nonrare_chemicalbits = []
for feat_iter in range(len(feature_samplenum)):
    print(feat_iter)
    if(feature_samplenum[feat_iter]/interactuion_num > 0.0001 and feat_iter < 1024):
        nonrare_chemicalbits.append(feat_iter)
    elif(feature_samplenum[feat_iter]/interactuion_num > 0.0001):
        nonrare_proteinbits.append(feat_iter-1024)


protein_filterted = protein
for protein_iter in range(len(protein_filterted.index.values)): #
    print(protein_iter)
    protein_filterted['protein_feature_bitvec'][protein_iter] = ''.join([protein_filterted['protein_feature_bitvec'][protein_iter][nonrare_iter] for nonrare_iter in nonrare_proteinbits])

protein_filterted.to_csv('data/derived/proteins_filteredbits_0.01percent_allstitch.csv')

