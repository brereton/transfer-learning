import pandas as pd
###
def space_init(target_space):
    '''
    Generating the features and specifying the network architecture for chemical or protein space
    :param target_space: name of target space
    :return: list of features and layer sizes
    '''
    if target_space == 'chemical':
        chemical = pd.read_table('data/derived/chemical_bitvecs.tsv', index_col=0)
        chemical_bitveclist = [list(iter) for iter in chemical['chemical_bitvec']]
        bitvec_list = chemical_bitveclist
        layer_list = [256, 128, 64]
    elif target_space == 'protein':
        protein = pd.read_table('data/derived/protein_bitvecs.tsv', index_col=0)
        protein_bitveclist = [list(iter) for iter in protein['protein_feature_bitvec']]
        bitvec_list = protein_bitveclist
        layer_list = [2048, 512, 128]

    return bitvec_list, layer_list